var express = require('express');
var router = express.Router();

Category = require('../models/category.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/add', function(req, res, next) {
  res.render('add_website');
});

router.post('/add', function(req, res, next) {
  var website = new Website();
  website.title = req.body.title;
  website.url = req.body.url;
  website.description = req.body.description;
  website.save(function(err) {
    if (err) {
      res.send(err);
    } else {
      res.redirect('/');
    }
  });
});

module.exports = router;
