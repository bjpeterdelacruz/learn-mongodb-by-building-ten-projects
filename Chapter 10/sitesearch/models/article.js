var mongoose = require('mongoose');
var searchPlugin = require('mongoose-search-plugin');
var Schema = mongoose.Schema;

// Article Schema
var articleSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

articleSchema.plugin(searchPlugin, {
    fields: ['title', 'description', 'url', 'category', 'author']
});

var Article = module.exports = mongoose.model('Article', articleSchema);

module.exports.searchArticles = function(searchText, callback, limit) {
    Article.search(searchText, {title: 1, description: 1, url: 1, category: 1, author: 1}, {
        conditions: {title: {$exists: true}, description: {$exists: true}, url: {$exists: true}, category: {$exists: true}, author: {$exists: true}},
        sort: {title: 1},
        limit: 50
    }, callback);
};

module.exports.addArticle = function(article, callback) {
    Article.save(article, callback);
};