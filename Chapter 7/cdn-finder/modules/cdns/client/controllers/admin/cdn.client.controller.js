﻿(function () {
  'use strict';

  angular
    .module('cdns.admin')
    .controller('CdnsAdminController', CdnsAdminController);

  CdnsAdminController.$inject = ['$scope', '$state', '$window', 'cdnResolve', 'Authentication', 'Notification'];

  function CdnsAdminController($scope, $state, $window, cdn, Authentication, Notification) {
    var vm = this;

    vm.cdn = cdn;
    vm.authentication = Authentication;
    vm.form = {};
    vm.remove = remove;
    vm.save = save;

    // Remove existing CDN
    function remove() {
      if ($window.confirm('Are you sure you want to delete?')) {
        vm.cdn.$remove(function() {
          $state.go('admin.cdns.list');
          Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> CDN deleted successfully!' });
        });
      }
    }

    // Save CDN
    function save(isValid) {
      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'vm.form.cdnForm');
        return false;
      }

      // Create a new CDN, or update the current instance
      vm.cdn.createOrUpdate()
        .then(successCallback)
        .catch(errorCallback);

      function successCallback(res) {
        $state.go('admin.cdns.list'); // should we send the User to the list or the updated CDN's view?
        Notification.success({ message: '<i class="glyphicon glyphicon-ok"></i> CDN saved successfully!' });
      }

      function errorCallback(res) {
        Notification.error({ message: res.data.message, title: '<i class="glyphicon glyphicon-remove"></i> CDN save error!' });
      }
    }
  }
}());
