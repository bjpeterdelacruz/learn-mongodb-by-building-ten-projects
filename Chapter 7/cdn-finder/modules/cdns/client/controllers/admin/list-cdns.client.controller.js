﻿(function () {
  'use strict';

  angular
    .module('cdns.admin')
    .controller('CdnsAdminListController', CdnsAdminListController);

  CdnsAdminListController.$inject = ['$scope', 'CdnsService'];

  function CdnsAdminListController($scope, CdnsService) {
    var vm = this;

    vm.cdns = CdnsService.query();

    $scope.filterOptions = {
      types: [
        {name: 'Show All'},
        {name: 'Javascript'},
        {name: 'CSS'}
      ]
    };

    $scope.filterItem = {
      type: $scope.filterOptions.types[0]
    };

    $scope.customFilter = function(data) {
      if (data.type === $scope.filterItem.type.name) {
        return true;
      } else if ($scope.filterItem.type.name === 'Show All') {
        return true;
      } else {
        return false;
      }
    }

  }
}());
