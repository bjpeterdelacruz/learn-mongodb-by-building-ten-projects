(function () {
  'use strict';

  angular
    .module('cdns')
    .controller('CdnsController', CdnsController);

  CdnsController.$inject = ['$scope', 'cdnResolve', 'Authentication'];

  function CdnsController($scope, cdn, Authentication) {
    var vm = this;

    vm.cdn = cdn;
    vm.authentication = Authentication;

  }
}());
