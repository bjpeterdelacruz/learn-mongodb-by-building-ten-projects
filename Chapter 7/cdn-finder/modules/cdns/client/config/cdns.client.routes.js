(function () {
  'use strict';

  angular
    .module('cdns.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('cdns', {
        abstract: true,
        url: '/',
        template: '<ui-view/>'
      })
      .state('cdns.list', {
        url: '',
        templateUrl: '/modules/cdns/client/views/list-cdns.client.view.html',
        controller: 'CdnsListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Libraries List'
        }
      })
      .state('cdns.view', {
        url: 'cdns/:cdnId',
        templateUrl: '/modules/cdns/client/views/view-cdn.client.view.html',
        controller: 'CdnsController',
        controllerAs: 'vm',
        resolve: {
          cdnResolve: getCdn
        },
        data: {
          pageTitle: 'CDN {{ cdnResolve.title }}'
        }
      });
  }

  getCdn.$inject = ['$stateParams', 'CdnsService'];

  function getCdn($stateParams, CdnsService) {
    return CdnsService.get({
      cdnId: $stateParams.cdnId
    }).$promise;
  }
}());
