﻿(function () {
  'use strict';

  angular
    .module('cdns.admin.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('admin.cdns', {
        abstract: true,
        url: '/cdns',
        template: '<ui-view/>'
      })
      .state('admin.cdns.list', {
        url: '',
        templateUrl: '/modules/cdns/client/views/admin/list-cdns.client.view.html',
        controller: 'CdnsAdminListController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        }
      })
      .state('admin.cdns.create', {
        url: '/create',
        templateUrl: '/modules/cdns/client/views/admin/form-cdn.client.view.html',
        controller: 'CdnsAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          cdnResolve: newCdn
        }
      })
      .state('admin.cdns.edit', {
        url: '/:cdnId/edit',
        templateUrl: '/modules/cdns/client/views/admin/form-cdn.client.view.html',
        controller: 'CdnsAdminController',
        controllerAs: 'vm',
        data: {
          roles: ['admin']
        },
        resolve: {
          cdnResolve: getCdn
        }
      });
  }

  getCdn.$inject = ['$stateParams', 'CdnsService'];

  function getCdn($stateParams, CdnsService) {
    return CdnsService.get({
      cdnId: $stateParams.cdnId
    }).$promise;
  }

  newCdn.$inject = ['CdnsService'];

  function newCdn(CdnsService) {
    return new CdnsService();
  }
}());
