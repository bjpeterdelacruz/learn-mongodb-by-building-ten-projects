﻿(function () {
  'use strict';

  // Configuring the CDNs Admin module
  angular
    .module('cdns.admin')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(Menus) {
    Menus.addSubMenuItem('topbar', 'admin', {
      title: 'Manage Libraries',
      state: 'admin.cdns.list'
    });
  }
}());
