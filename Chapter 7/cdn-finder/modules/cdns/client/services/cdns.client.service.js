(function () {
  'use strict';

  angular
    .module('cdns.services')
    .factory('CdnsService', CdnsService);

  CdnsService.$inject = ['$resource', '$log'];

  function CdnsService($resource, $log) {
    var Cdn = $resource('/api/cdns/:cdnId', {
      cdnId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });

    angular.extend(Cdn.prototype, {
      createOrUpdate: function () {
        var cdn = this;
        return createOrUpdate(cdn);
      }
    });

    return Cdn;

    function createOrUpdate(cdn) {
      if (cdn._id) {
        return cdn.$update(onSuccess, onError);
      } else {
        return cdn.$save(onSuccess, onError);
      }

      // Handle successful response
      function onSuccess(cdn) {
        // Any required internal processing from inside the service, goes here.
      }

      // Handle error response
      function onError(errorResponse) {
        var error = errorResponse.data;
        // Handle error internally
        handleError(error);
      }
    }

    function handleError(error) {
      // Log error
      $log.error(error);
    }
  }
}());
