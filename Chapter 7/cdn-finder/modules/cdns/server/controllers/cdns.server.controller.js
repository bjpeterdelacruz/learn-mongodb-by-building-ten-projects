'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  CDN = mongoose.model('CDN'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));

/**
 * Create a CDN
 */
exports.create = function (req, res) {
  var cdn = new CDN(req.body);
  cdn.user = req.user;

  cdn.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(cdn);
    }
  });
};

/**
 * Show the current CDN
 */
exports.read = function (req, res) {
  // convert mongoose document to JSON
  var cdn = req.cdn ? req.cdn.toJSON() : {};

  // Add a custom field to the CDN, for determining if the current User is the "owner".
  // NOTE: This field is NOT persisted to the database, since it doesn't exist in the CDN model.
  cdn.isCurrentUserOwner = !!(req.user && cdn.user && cdn.user._id.toString() === req.user._id.toString());

  res.json(cdn);
};

/**
 * Update a CDN
 */
exports.update = function (req, res) {
  var cdn = req.cdn;

  cdn.name = req.body.name;
  cdn.url = req.body.url;
  cdn.description = req.body.description;
  cdn.version = req.body.version;
  cdn.type = req.body.type;

  cdn.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(cdn);
    }
  });
};

/**
 * Delete a CDN
 */
exports.delete = function (req, res) {
  var cdn = req.cdn;

  cdn.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(cdn);
    }
  });
};

/**
 * List of CDNs
 */
exports.list = function (req, res) {
  CDN.find().sort('-created').populate('user', 'displayName').exec(function (err, cdns) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(cdns);
    }
  });
};

/**
 * CDN middleware
 */
exports.cdnByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'CDN is invalid'
    });
  }

  CDN.findById(id).populate('user', 'displayName').exec(function (err, cdn) {
    if (err) {
      return next(err);
    } else if (!cdn) {
      return res.status(404).send({
        message: 'No CDN with that identifier has been found'
      });
    }
    req.cdn = cdn;
    next();
  });
};
