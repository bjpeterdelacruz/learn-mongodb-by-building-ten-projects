var myApp = angular.module('myApp', []);

myApp.controller('AppController', ['$scope', '$http', function($scope, $http) {
    $scope.stateCode = 'HI';

    $scope.getAirports = function() {
        $http.get('/api/airports').then(function(response) {
            $scope.airports = response.data;
        });
    }

    $scope.findAirports = function() {
        $http.get('/api/airports/state/' + $scope.stateCode).then(function(response) {
            $scope.airports = response.data;
        });
    }

    $scope.findAirportsByProx = function() {
        var location = {
            distance: $scope.location.distance
        }
        $http.get('/geocode/location?address=' + $scope.location.address).then(function(loc_response) {
            location.lat = loc_response.data.locations[0].latitude;
            location.lon = loc_response.data.locations[0].longitude;

            $http.post('/api/airports/prox', location).then(function(response) {
                $scope.airports = response.data;
            });
        });
    }
}]);