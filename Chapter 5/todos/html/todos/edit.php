<?php include('includes/header.php'); ?>

<?php $id = $_GET['id'] ?>

<?php $todo = new Todo($m, $collection); ?>

<?php
  if (isset($_POST['submit'])) {
  	$todo->updateTodo($id, $_POST);
  }
?>

<?php $currentTodo = $todo->getTodo($id); ?>

<form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
  <div class="form-group">
    <label>Name</label>
    <input type="text" class="form-control" name="name" value="<?php echo $currentTodo->name; ?>" placeholder="Name">
  </div>
  <div class="form-group">
    <label>Category</label>
    <select class="form-control" name="category">
      <option value="Work" <?php if($currentTodo->category == "Work") { echo 'selected'; } ?>>Work</option>
      <option value="Family" <?php if($currentTodo->category == "Family") { echo 'selected'; } ?>>Family</option>
      <option value="Other" <?php if($currentTodo->category == "Other") { echo 'selected'; } ?>>Other</option>
    </select>
  </div>
  <div class="form-group">
    <label>Priority</label>
    <select class="form-control" name="priority">
      <option value="Low" <?php if($currentTodo->priority == "Low") { echo 'selected'; } ?>>Low</option>
      <option value="Normal" <?php if($currentTodo->priority == "Normal") { echo 'selected'; } ?>>Normal</option>
      <option value="High" <?php if($currentTodo->priority == "High") { echo 'selected'; } ?>>High</option>
    </select>
  </div>
  <div class="form-group">
    <label>Description</label>
    <textarea class="form-control" name="description"><?php echo $currentTodo->description; ?></textarea>
  </div>
  <div class="form-group">
    <label>Due Date</label>
    <input type="date" value="<?php echo $currentTodo->task_date; ?>" class="form-control" name="task_date">
  </div>
  <button type="submit" class="btn btn-default" name="submit">Submit</button>
</form>

<?php include('includes/footer.php'); ?>