<?php include('includes/header.php'); ?>

<?php $id = $_GET['id'] ?>

<?php $todo = new Todo($m, $collection); ?>

<?php
  if (isset($_POST['deleteSubmit'])) {
  	$todo->removeTodo($id);
  }
?>

<?php $currentTodo = $todo->getTodo($id); ?>

<h1><?php echo $todo->getTodo($id)->name ?></h1>

<ul class="list-group">
	<li class="list-group-item">Category: <?php echo $currentTodo->category ?></li>
	<li class="list-group-item">Date:
	<?php
	  $task_date = $currentTodo->task_date;
	  if (is_string($task_date)) {
	  	echo $task_date;
	  } else {
	    echo $task_date->toDateTime()->format('Y-m-d');
	  }
	?></li>
	<li class="list-group-item">Priority: <?php echo $currentTodo->priority ?></li>
</ul>
<h4>Todo Description</h4>
<p class="well"><?php echo $todo->getTodo($id)->description ?></p>

<a class="btn btn-primary" href="/todos/edit.php?id=<?php echo $id; ?>">Edit</a>
<form class="pull-right" action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
    <input type="hidden" value="<?php echo $id; ?>" name="idField">
	<input type="submit" name="deleteSubmit" value="Delete" class="btn btn-danger">
</form>

<?php include('includes/footer.php'); ?>