<?php include('includes/header.php'); ?>

<?php $todo = new Todo($m, $collection); ?>

<h1>My Todos</h1>
<ul class="list-group">
    <?php foreach($todo->getTodos() as $todo) : ?>
    	<li class="list-group-item"><a href="todo.php?id=<?php echo $todo->_id; ?>"><?php echo "$todo->name"; ?></a></li>
    <?php endforeach; ?>
</ul>

<?php include('includes/footer.php'); ?>