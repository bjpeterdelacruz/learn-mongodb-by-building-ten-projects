<?php
  error_reporting(E_ALL);
  ini_set('display_errors', 1);
?>

<?php include('lib/db.php'); ?>
<?php include('classes/todo.php'); ?>

<!DOCTYPE>
<html>
  <head>
    <title>My Todo List</title>
    <link rel="stylesheet" href="public/css/bootstrap.min.css">
  </head>
  <body>

    <nav class="navbar navbar-inverse">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/todos/index.php">My Todo List</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="/todos/index.php">Home</a></li>
            <li><a href="/todos/add.php">Add Todo</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">