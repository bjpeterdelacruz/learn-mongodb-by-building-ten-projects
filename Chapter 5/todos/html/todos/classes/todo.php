<?php

class Todo {
	protected $db;
	protected $collection;
	protected $document;
	private $name;
	private $category;
	private $priority;
	private $description;
	private $task_date;

    public function __construct($db, $collection) {
    	$this->db = $db;
    	$this->collection = $collection;
    }

    public function getTodos() {
    	return $this->collection;
    }

    public function getTodo($id) {
    	$filter = ['_id' => new MongoDB\BSON\ObjectId($id)];
    	$query = new MongoDB\Driver\Query($filter);
    	return $this->db->executeQuery('test.todos', $query)->toArray()[0];
    }

    public function addTodo($post) {
    	$this->document = $this->createDocument($post);
        $bulk = new MongoDB\Driver\BulkWrite;
        $bulk->insert($this->document);
        $this->db->executeBulkWrite('test.todos', $bulk);
        header('Location: index.php');
    }

    public function removeTodo($id) {
        $this->document = array(
            "_id" => new MongoDB\BSON\ObjectId($id)
        );
        $bulk = new MongoDB\Driver\BulkWrite;
        $bulk->delete($this->document);
        $this->db->executeBulkWrite('test.todos', $bulk);
        header('Location: index.php');
    }

    public function updateTodo($id, $post) {
        $this->document = $this->createDocument($post);
        $bulk = new MongoDB\Driver\BulkWrite;
        $bulk->update(
            [ "_id" => new MongoDB\BSON\ObjectId($id) ], $this->document
        );
        $this->db->executeBulkWrite('test.todos', $bulk);
        header('Location: index.php');
    }

    private function createDocument($post) {
        $this->name = $post['name'];
        $this->category = $post['category'];
        $this->priority = $post['priority'];
        $this->description = $post['description'];
        $this->task_date = $post['task_date'];

        return array(
            "name" => $this->name,
            "category" => $this->category,
            "priority" => $this->priority,
            "description" => $this->description,
            "task_date" => $this->task_date
        );
    }
}