<?php include('includes/header.php'); ?>

<?php
  if (isset($_POST['submit'])) {
  	$todo = new Todo($m, $collection);
  	$todo->addTodo($_POST);
  }
?>
<form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
  <div class="form-group">
    <label>Name</label>
    <input type="text" class="form-control" name="name" placeholder="Name">
  </div>
  <div class="form-group">
    <label>Category</label>
    <select class="form-control" name="category">
      <option value="Work">Work</option>
      <option value="Family">Family</option>
      <option value="Other">Other</option>
    </select>
  </div>
  <div class="form-group">
    <label>Priority</label>
    <select class="form-control" name="priority">
      <option value="Low">Low</option>
      <option value="Normal" selected>Normal</option>
      <option value="High">High</option>
    </select>
  </div>
  <div class="form-group">
    <label>Description</label>
    <textarea class="form-control" name="description"></textarea>
  </div>
  <div class="form-group">
    <label>Due Date</label>
    <input type="date" class="form-control" name="task_date">
  </div>
  <button type="submit" class="btn btn-default" name="submit">Submit</button>
</form>

<?php include('includes/footer.php'); ?>