# Learn MongoDB by Building Ten Projects

## About

This repository contains source code for ten different MongoDB projects.

## Project Descriptions

**Chapter 1: Customer Database**

Sample [MongoDB](http://www.mongodb.com) commands that can be executed in the `mongo` shell.

**Chapter 2: Catalog API**

An [Express](http://expressjs.com) application that stores and serves product information.

> __Live:__ [catalogapi.herokuapp.com](http://catalogapi.herokuapp.com)

**Chapter 3: Task Manager**

A [Node.js](https://nodejs.org/en) application that people can use to keep track of tasks.

> __Live:__ [tinyurl.com/task-manager-bpd](http://tinyurl.com/task-manager-bpd)

**Chapter 4: Photoz**

A [Meteor](http://www.meteor.com) application that displays uploaded pictures in a photo gallery.

> __Live:__ [phootoz.herokuapp.com](http://phootoz.herokuapp.com)

**Chapter 5: My Todo List**

A [PHP 7](http://www.php.net) application that keeps track of todos.

> __Live:__ [phptodos.herokuapp.com](http://phptodos.herokuapp.com)

**Chapter 6: Invoicr**

A client management system.

> __Live:__ [cmsinvoicr.herokuapp.com](http://cmsinvoicr.herokuapp.com)

**Chapter 7: CDNFinder**

A CDN repository that was implemented using the [MEAN.JS](https://meanjs.org) framework.

> __Live:__ [cdnfind.herokuapp.com](http://cdnfind.herokuapp.com)

**Chapter 8: AirFind Airport Directory**

An Express application that uses the [Geocoding API](https://www.npmjs.com/package/express-geocoding-api) to find and display airports across the United States.

> __Live:__ [airfind.herokuapp.com](http://airfind.herokuapp.com)

**Chapter 9: MongoChat**

A chat client that uses the [Socket.IO](http://socket.io) library to send messages.

> __Live:__ [mongochat.herokuapp.com](http://mongochat.herokuapp.com)

**Chapter 10: SiteSearch**

An Express application that uses the [Mongoose Search Plugin](https://www.npmjs.com/package/mongoose-search-plugin) to search the database for websites and news articles.

> __Live:__ [sitesearch10.herokuapp.com](http://sitesearch10.herokuapp.com)
