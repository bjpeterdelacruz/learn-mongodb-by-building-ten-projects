var myApp = angular.module('myApp');

myApp.controller('InvoicesController', ['$scope', '$http', '$location', '$routeParams', function($scope, $http, $location, $routeParams) {
  $scope.getCustomers = function() {
    $http.get('/api/customers').success(function(response) {
      console.log(response);
      $scope.customers = response;
    });
  }

  $scope.getInvoices = function() {
    $http.get('/api/invoices').success(function(response) {
      console.log(response);
      $scope.invoices = response;
    });
  }

  $scope.getInvoice = function() {
    var id = $routeParams.id;
    $http.get('/api/invoices/' + id).success(function(response) {
      console.log(response);
      $scope.invoice = response;
      // Fill select
      $scope.invoice.customer = response.customer;
      $scope.invoice.status = response.status;
    });
  }

  $scope.addInvoice = function() {
    $http.post('/api/invoices/', $scope.invoice).success(function(response) {
      console.log(response);
      window.location.href = '/#invoices';
    });
  }

  $scope.updateInvoice = function() {
    $http.put('/api/invoices/' + $scope.invoice._id, $scope.invoice).success(function(response) {
      console.log(response);
      window.location.href = '/#invoices';
    });
  }

  $scope.deleteInvoice = function(id) {
    $http.delete('/api/invoices/' + id, $scope.customer).success(function(response) {
      console.log(response);
      window.location.href = '/#invoices';
    });
  }
}]);
